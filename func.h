#ifndef __FUNC_H_
#define __FUNC_H_
#include "exam.h"
#include <set>
#include <vector>
#include <exception>


std::vector<std::pair<Question,float>> find_matches(std::vector<Exam> const &exams,
                                       std::set<std::string> const &dump,
                                       float match_percent);
std::set<std::string> read_dump_file(std::string const &name);
std::ostream& operator<<(std::ostream &, Question const &);
#endif
