#include "func.h"


std::vector<std::pair<Question,float>> find_matches(std::vector<Exam> const &exams,
                                       std::set<std::string> const &dump,
                                       float match_percent)
{
   std::vector<std::pair<Question,float>> matched_questions;
   for(const auto &exam : exams)
   {
      for(const auto &question : exam.getQuestions())
      {
         std::size_t count{};
         for(auto const &word : question.getDump())
            if(dump.contains(word)) ++count;
         if(auto ratio = static_cast<float>(count) / question.getDump().size(); ratio >= match_percent)
            matched_questions.emplace_back(question, ratio);
      }
   }
   return matched_questions;
}


std::set<std::string> read_dump_file(std::string const &name)
{
   std::ifstream input{name};
   if(not input)
      throw std::runtime_error("Could not open file \""+name+"\": "+strerror(errno));

   std::set<std::string> dump;
   while(input)
   {
      std::string line;
      std::getline(input, line);
      auto words = split(line, " \t\r\n");
      dump.insert(words.begin(), words.end());
   }
   return dump;
}


std::ostream& operator<<(std::ostream &out, Question const &q)
{
   for(auto const &str : q.getText())
      out << str << '\n';
   return out;
}
