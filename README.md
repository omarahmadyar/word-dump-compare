# What is this repo?
The purpose of this project is to take word dumps and try to determine what questions will be on the exam.

## What is a word dump?
Professor Mackey creates "word dumps" for every exam.  To generate a word dump, he runs a bash script which takes every single word from the midterm, converts it to lower case, sorts it, and removes unique entries.  
ex: cat midterm | tr A-Z a-z | tr -c a-z '\n' | sort | uniq | fmt -60

# How do I use this program?
$ dumpcomp [options] {provided\_word\_dump} {oldexams}

ex: dumpcomp -r .7 dump.txt OldExams/\*midterm\*.tt

## Where do I get old exams
Idk.  Maybe the OldExams folder on the course website?  
Use scp to download them all at once.  
$ scp -r {cruzID}@{server}:{PWD to OldExams} .

# What does it tell me?
Definitively?  Nothing.  But it'll highlight potential problems that could be on the exam.
