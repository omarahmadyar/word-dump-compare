CC=g++
CFLAGS=-std=c++20 -Wall -Wextra -O3

EXE=dumpcomp

all: $(EXE)

$(EXE): exam.o main.o func.o util.o
	$(CC) -o $@ $^

%.o: %.cpp
	$(CC) $(CFLAGS) -c $<

tidy:
	$(RM) *.o

clean: tidy
	$(RM) $(EXE)
