#ifndef __DUMP_H_
#define __DUMP_H_
#include <fstream>
#include <vector>
#include <set>
#include <regex>
#include <exception>
#include <errno.h>
#include "util.h"

template<typename T>
concept strvec_t = std::same_as<std::decay_t<T>, std::vector<std::string>>;

class Question
{
   std::vector<std::string> raw;
   std::set<std::string> dump;
   std::string exam_name;
public:
   static bool cut_blocks; //tries to cut out ascii blocks/rectangles from questions

private:
   void compileDump();
public:
   Question(strvec_t auto &&raw_, std::string const &ename)
           : raw(std::forward<decltype(raw_)>(raw_)), exam_name(ename) { compileDump(); }
   const decltype(raw) & getText() const  {return raw;}
   const decltype(dump)& getDump() const {return dump;}
   const decltype(exam_name)& getExam() const {return exam_name;}
};


class Exam
{
   std::vector<Question> questions;
   std::string name;

public:
   static bool nomult; //don't parse multiple choice questions
private:
   void parse_file(std::string const & file_name);
public:
   Exam(std::string const & file_name);
   const decltype(questions) & getQuestions() const {return questions;}
   const decltype(name) & getName() const {return name;}
};

#endif
