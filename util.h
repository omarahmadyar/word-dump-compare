#ifndef __UTIL_H_
#define __UTIL_H_
#include <vector>
#include <string>
std::vector<std::string> split(std::string const & str, std::string const & delim);
#endif
