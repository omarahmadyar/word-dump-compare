#include "util.h"

std::vector<std::string> split(std::string const & str, std::string const & delim)
{
   std::vector<std::string> ret;
   std::decay_t<decltype(std::string::npos)> pos{};
   while(pos != std::string::npos)
   {
      auto beg = str.find_first_not_of(delim, pos);
      if(beg == std::string::npos) break;
      auto end = str.find_first_of(delim, beg);
      ret.emplace_back(str.substr(beg, end-beg));
      pos = end;
   }
   return ret;
}
