#include "exam.h"
#ifdef DEBUG
#include<iostream>
#endif
bool Question::cut_blocks = false;
bool Exam::nomult = false;


void Question::compileDump()
{
   for(auto line : raw)
   {
      if(Question::cut_blocks)
      {
         line = line.substr(0, line.find("---"));
         line = line.substr(0, line.find("+-"));
         line = line.substr(0, line.find("|"));
      }

      for(auto &chr : line)
         if(not isalpha(chr)) chr = ' ';
         else chr = tolower(chr);

      auto words = split(line, " \t\n\r");
      this->dump.insert(words.begin(), words.end());
   }
}


void Exam::parse_file(std::string const & file_name)
{
   const static std::regex qstart{R"(^\s*\d\d?\.\s.*$)"}; //match start of question
   const static std::regex blankLine{R"(^\s*$)"}; // match empty line
   const static std::regex multchoice{R"(^.*[mM]ultiple [Cc]hoice.*$)"};//multiple choice

   // Setup
   std::ifstream input{file_name};
   if(not input)
      throw std::runtime_error("Could not open file \""+file_name+"\": "+strerror(errno));
   std::vector<std::string> qstring;
   bool started{false};
   bool prev_blank{false};

   // Loop over every line
   while(input)
   {
      std::string line;
      bool blank{false};
      bool intermission{false};

      std::getline(input, line);
      if(std::regex_match(line, multchoice)) intermission = true;
      if(std::regex_match(line, blankLine)) blank = true;
      else if((prev_blank and std::regex_match(line, qstart)) or intermission)
      {
         if(started)
         {
            #ifdef DEBUG
            std::cout << "Found Question:\n";
            for(auto const & elem: qstring)
               std::cout << elem << '\n';
            #endif
            questions.emplace_back(std::move(qstring), this->name);
         }
         qstring.clear();
         started = true;
         if(intermission)
         {
            if(Exam::nomult) break;
            else started = false;
         }
      }

      qstring.emplace_back(std::move(line));
      prev_blank = blank;
   }

   // Save last question
   if(qstring.size() > 0) questions.emplace_back(std::move(qstring), this->name);
}


Exam::Exam(std::string const & file_name) : name(file_name)
{
   parse_file(file_name);
}

