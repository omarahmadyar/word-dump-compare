#include <iostream>
#include <vector>
#include "exam.h"
#include "func.h"
#include <getopt.h>

static constexpr auto help =
R"||( {dump.txt} {oldexam.tt}...

Call the program with the first param being the provided word dump for the
upcoming exam, then provide it as many former exams as you have.

ex usage: ./dumpcomp provided_dump.txt oldexams/*.tt

Optional Flags:
-a to not cut out information that may be unnecessary (cuts out ascii blocks)
-r {float}  set the required percent for a question to be matched (default 0.75)
-m include multiple choice questions
-s sort questions in order of most matched to least matched
)||";

int main(const int argc, char *argv[])
{
   float match_percent = 0.75f;
   Question::cut_blocks = true;
   Exam::nomult = true;
   bool sorted = false;

   int opt;
   while((opt = getopt(argc, argv, "ar:ms")) != -1)
   {
      switch(opt)
      {
         case 'a': Question::cut_blocks = false; continue;
         case 'r': match_percent = strtof(optarg, nullptr); continue;
         case 'm': Exam::nomult = false; continue;
         case 's': sorted = true; continue;
      }
   }

   if(argc - optind < 2)
   {
      std::cerr << argv[0] << help;
      return 5;
   }

   auto provided_dump = read_dump_file(argv[optind++]);
   std::vector<std::string> old_exam_names(argv+optind, argv+argc);
   std::vector<Exam> old_exams(old_exam_names.cbegin(), old_exam_names.cend());
   auto potential_questions = find_matches(old_exams, provided_dump, match_percent);

   if(sorted)
      std::sort(  std::begin(potential_questions)
               ,  std::end(potential_questions)
               ,  [] (auto const &x, auto const &y) { return x.second > y.second; }
               );

   for(auto const &q : potential_questions)
      std::cout << "From Exam " << q.first.getExam() << " w/ match " << q.second <<
                   ":\n" << q.first;
   return 0;
}
